1. Introduction
In this lesson, we'll be expanding our vocabulary, and making simple sentences.

First, there are two lists of ten words, giving the singular and plural forms of each word.
      The first list is words for people, like "ji'nm" ("man") and "ji'nmug" ("men").
                     *to make these words plural, you add "-g" or "-q" to the end!
      The second list is words for things, like "awti" ("road") and "awtil" ("roads").
                     *to make these words plural, you add "-l" or "-n" to the end!
      Practice saying these words as you listen to the recording.
             (Remember you can pause or slow down the recording if needed.)

Second, you'll hear and create sentences using the verbs "nemi'g", "nemi'gig", "nemitu", and "nemituann", meaning "I see [something]".


2. "I see..."
Mi'gmaq is very precise in each of its words--the verb "to see" changes its endings depending on who is seeing something, and what it is that they're seeing.In the next few screens, we'll be hearing and saying sentences of the type
                "I see [a person/thing].

"nemi-- "I see..."
(what:)               ________________________________________________________
                      nemi'g    |  gisigu, e'pit, ji'nmji'j, mijuaji'j...     
(people, singular)    __________|_____________________________________________
                      nemi'gig  |  gisigug, e'pijig, ji'nmji'jg, mijuaji'jg...
(people, plural)      __________|_____________________________________________
                      nemitu    |  twopeti, gaqan, mutputi, wenjiguom...        
(things, singular)    __________|_____________________________________________
                      nemituann |  twopetil, gaqann, mutputil, wenjiguoml...    
(things, plural)      __________|_____________________________________________                                                                               



3. About -igtug
To say "at/near/by/on a location" in Mi'gmaq is very simple: just take the word you want to be a location, and add the appropriate "-igtug" ending.
                            The "-igtug" endings:
                               -igtug
                                -gtug
                                 -tug
                                  -ug
                                   -g

 Sometimes the location word has letters in common with "-igtug".
 In that case, they cancel out, like in "patautigtug" and "sipugtug" below:

   guntal      "rocks         |  patauti       "table"        |  sipug    "river" +       igtug                | +      igtug                  | +   igtug
 = guntaligtug "on the rocks" | =patautigtug   "by the table" | =sipugtug "at the river"